import unittest
from Cochecodigo import Coche 

class TestCoche(unittest.TestCase):
    def test_acelera(self):
        coche = Coche("Verde", "Citroen","C5", "1206ICB", veloc=40)
        coche.acelera(10)
        self.assertEqual(coche.velocidad, 50)#compara los datos que hemos introducido con lo que queremos que de 
    def test_frena(self):
        coche = Coche("Azul", "Toyota" , "Corolla","2939LKM", veloc=30)
        coche.frena(10)
        self.assertEqual(coche.velocidad, 20)

if __name__== "__main__":
    unittest.main()